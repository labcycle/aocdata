use std::path::Path;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let proto_dir = &Path::new(std::env!("CARGO_MANIFEST_DIR")).join("proto");

    let mut config = prost_build::Config::new();
    config.protoc_arg("--experimental_allow_proto3_optional");

    tonic_build::configure().compile_with_config(
        config,
        &[proto_dir.join("aocdata.proto")],
        &[proto_dir],
    )?;

    Ok(())
}
