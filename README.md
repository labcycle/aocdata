# Aocdata

Aocdata provides data service i.e. datasets for [Advent Of Code](https://adventofcode.com) puzzles.
The service, the request format, and the response format are specified in [`aocdata.proto`](proto/aocdata.proto) file following the [Proto3](https://protobuf.dev/programming-guides/proto3) language guide.

# Setup
```
$ git clone https://gitlab.com/labcycle/aocdata.git
$ cd aocdata
```
The below commands simulate the database and the server through container images.
Replace `podman` with `docker` in the following commands to build and run images. NOTE: The `build` and `run` commands have NOT been tested using `docker`.

## Setup Postgresql database
This database holds datasets of the puzzles. If the database is already setup, skip to [Setup Aocdata server](#setup-aocdata-server) section.

NOTE: An example database holding sample datasets is set up below. For more extensive datasets, a new database with datasets should to be set up.

```
$ podman build -t aocdb -f ./db/Containerfile
$ podman run -dt --name aocdb_container -p 54320:5432 -e POSTGRES_PASSWORD="aocpg" aocdb 
``` 

## Setup Aocdata server
Postgresql database URL must be provided as an environment variable `AOC_DB_URL` to the container. e.g. `AOC_DB_URL="postgres://postgres:aocpg@localhost:54320/aoc"`.

```
$ podman build -t aocdata -f ./Containerfile
$ podman run -dt --name aocdata_container -p 31370:3137 -e AOC_DB_URL=__PostgresDbUrl__ aocdata
```

# How does it work
- Once the server is started, data can be requested using gRPC call using the unary RPC `GetData` and client-side `DataRequest` mentioning the year and day fields.
- The data response is sent as an optional string.
- The dataset string is queried from a Postgresql database (Thus, the database must be running as long as the server is taking requests).

