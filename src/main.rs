#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    aocdata::run().await?;

    Ok(())
}
