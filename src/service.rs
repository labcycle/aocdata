use sea_orm::{DatabaseConnection, EntityTrait};
use tonic::{Code, Request, Response, Status};

use crate::entities::prelude::*;

use crate::aocdata::{aoc_data_server::AocData, DataRequest, DataResponse};

#[derive(Debug)]
pub(crate) struct AocDataService {
    connection: DatabaseConnection,
}

impl AocDataService {
    pub(crate) fn with_db_conn(connection: DatabaseConnection) -> Self {
        AocDataService { connection }
    }
}

#[tonic::async_trait]
impl AocData for AocDataService {
    /// This method implements unary RPC to take a single data request and provide the puzzle
    /// dataset by querying the database against (year, day).
    async fn get_data(
        &self,
        request: Request<DataRequest>,
    ) -> Result<Response<DataResponse>, Status> {
        let DataRequest { year, day } = request.into_inner();
        let id = (year as i32, day as i32);

        Dataset::find_by_id(id)
            .one(&self.connection)
            .await
            .map(|model| {
                Response::new(DataResponse {
                    data: model.map(|value| value.data),
                })
            })
            .map_err(|err| Status::new(Code::Internal, format!("database error: {}", err)))
    }
}
