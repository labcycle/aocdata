\c aoc;

CREATE TABLE dataset(
	year	INT,
	day		INT,
	data 	TEXT,
	PRIMARY KEY (year, day)
);

\set content `cat 2022_01.txt`
INSERT INTO dataset VALUES(2022, 1, :'content');

\set content `cat 2022_02.txt`
INSERT INTO dataset VALUES(2022, 2, :'content');
